# An Introduction to Git

This is an introduction to Git. Git is a VCS (version control system) which we actually use in our everyday workflow. This is a very basic introduction to a few, essential commands. 

# Cheatsheets

On the cheatsheets folder you can find some very useful cheatsheets in printable form. Print them and place them on your desk, behind your monitor or wherever you believe you can have easy access to them when you are dealing with Git, until at least you are feeling comfortable with these command and the workflow. Images found on https://www.git-tower.com. 


